#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *

#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from .authorization import ambil_resource,ambil_token

from rest_framework.exceptions import PermissionDenied

class Examples(viewsets.ModelViewSet):
    serializer_class = ExampleSerializer
    queryset = Example.objects.all()
    def create(self, request) :
        form_serializer = ExampleSerializer(data=request.data)
        auth = request.session.get("Authorization")
        auth_response = ambil_resource(auth).json()
        if auth_response.get("access_token") != None :
            if form_serializer.is_valid():
                form_serializer.save()
            return Response(form_serializer.data, status=status.HTTP_201_CREATED)

        else:
            return Response({"Message": "silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def list(self, request) :
        auth = request.session.get("Authorization")
        auth_response = ambil_resource(auth).json()
        if auth_response.get("access_token") != None :
            qry = Example.objects.all()
            serializer = ExampleSerializer(qry, many=True)
            return Response(serializer.data)

        else:
            return Response({"Message": "silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def retrieve(self, request, pk) :
        auth = request.session.get("Authorization")
        auth_response = ambil_resource(auth).json()
        if auth_response.get("access_token") != None :
            instance = Example.objects.get(id=pk)
            serializer = ExampleSerializer(instance)
            return Response(serializer.data)
        return Response({"Message": "silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

        

    def update(self, request, *args, **kwargs):
        auth = request.session.get("Authorization")
        auth_response = ambil_resource(auth).json()
        if auth_response.get("access_token") != None :
            partial = kwargs.pop("partial", False)
            instance = self.get_object()
            instance.nama = request.data.get("nama")
            instance.npm = request.data.get("npm")
            instance.tempatTinggal = request.data.get("tempatTinggal")
            instance.tanggalLahir = request.data.get("tanggalLahir")
            instance.save()
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid()
            serializer.save()
            self.perform_update(serializer)
            if getattr(instance, '_prefetched_objects_cache', None):
                instance._prefetched_objects_cache = {}
                return Response(serializer.data)
            else:
                return Response(serializer.data)

        else:
            return Response({"Message": "silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def destroy(self, request, pk) :
        auth = request.session.get("Authorization")
        auth_response = ambil_resource(auth).json()
        if auth_response.get("access_token") != None :

            try:
                instance = self.get_object()
                self.perform_destroy(instance)
            except:
                pass
            return Response(status=status.HTTP_204_NO_CONTENT)

        return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)



class Login(viewsets.ModelViewSet):
    serializer_class = LoginSerializer
    queryset = {}

    def create(self,request):
        form_serializer = LoginSerializer(data=request.data)
        if form_serializer.is_valid():
            username = form_serializer.data["username"]
            password = form_serializer.data["password"]
            response = ambil_token(username, password, 'JVvQJG05oLQ2pZWKQEuiRAjkTExHPf4L', 'gl10UzzNdyNSS2n8BJbj7yuNu5f6vfDP')
            json_response = response.json()
            if json_response.get("access_token") != None:
                access_token = json_response['access_token']
                request.session["Authorization"] = access_token
                return Response({'Message':'login berhasil'}, status=status.HTTP_202_ACCEPTED)
        return Response({'Message':'username atau password salah'}, status=status.HTTP_400_BAD_REQUEST)