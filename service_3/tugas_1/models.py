from django.db import models
from django.contrib.auth.models import *
from datetime import timedelta
from datetime import date

class Example(models.Model):
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=128)
    npm = models.IntegerField(default=0)
    tempatTinggal = models.CharField(max_length=128,default="")
    tanggalLahir = models.DateField(default=date.today())
    createdDate = models.DateTimeField(editable=False)
    lastModifiedDate = models.DateTimeField(null=True,editable=False)
    def save(self, *args, **kwargs):
        if not self.id:
            self.createdDate = timezone.now()
        else:
            self.lastModifiedDate = timezone.now()
        return super(Example, self).save(*args, **kwargs)

    class Meta:
        db_table = 'example'
