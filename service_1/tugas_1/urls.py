from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from rest_framework.routers import DefaultRouter

from .controller import Examples,Login

router = DefaultRouter()
router.register(r'login',Login,basename='')
router.register(r'',Examples, basename='')


urlpatterns = [
    path('', include(router.urls)),
]