from django.db import models
from django.contrib.auth.models import *
from datetime import timedelta

class Example(models.Model):
    id = models.AutoField(primary_key=True)
    file = models.FileField(blank=False, null=False,upload_to="File/")
    createdDate = models.DateTimeField(editable=False)
    lastModifiedDate = models.DateTimeField(null=True,editable=False)
    def save(self, *args, **kwargs):
        if not self.id:
            self.createdDate = timezone.now()
        else:
            self.lastModifiedDate = timezone.now()
        return super(Example, self).save(*args, **kwargs)

    class Meta:
        db_table = 'example'
