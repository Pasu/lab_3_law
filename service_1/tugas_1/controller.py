#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *
from django.http import HttpResponse
from wsgiref.util import FileWrapper

#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from .authorization import *
import io
import zipfile

from rest_framework.exceptions import PermissionDenied

class Examples(viewsets.ModelViewSet):
    serializer_class = ExampleSerializer
    queryset = Example.objects.all()

    def list(self, request):
        auth = request.session.get("Authorization")
        auth_response = ambil_resource(auth).json()
        queryset = Example.objects.all()
        serializer = ExampleSerializer(queryset, many=True)
        if(auth_response.get("access_token") != None):
            return Response(serializer.data,status=status.HTTP_202_ACCEPTED)
        else:
            return Response({"Message":"silahkan login terlebih dahulu"},status=status.HTTP_401_UNAUTHORIZED)
        
    
    def create(self, request):
        form_serializer = ExampleSerializer(data=request.data)
        auth = request.session.get("Authorization")
        auth_response = ambil_resource(auth).json()
        if(auth_response.get("access_token") != None):
            if form_serializer.is_valid():
                form_serializer.save()
                filePath = form_serializer.data["file"]
                zip_buffer = io.BytesIO()
                fileName = filePath.split("/")
                fileName = fileName[-1]
                with zipfile.ZipFile("hasil zip/"+fileName+".zip", mode='a', compression=zipfile.ZIP_DEFLATED) as zip_file:
                    zip_file.write(filePath)
                zip_file = open("hasil zip/"+fileName+".zip", 'rb')
                response = HttpResponse(zip_file,content_type='application/zip')
                response['Content-Disposition'] = 'attachment; filename='+fileName+'.zip'
                return response
                #return Response(form_serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(form_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else: 
            return Response({"Message":"silahkan login terlebih dahulu"},status=status.HTTP_401_UNAUTHORIZED)

class Login(viewsets.ModelViewSet):
    serializer_class = LoginSerializer
    queryset = {}

    def create(self,request):
        form_serializer = LoginSerializer(data=request.data)
        if form_serializer.is_valid():
            username = form_serializer.data["username"]
            password = form_serializer.data["password"]
            response = ambil_token(username, password, 'JVvQJG05oLQ2pZWKQEuiRAjkTExHPf4L', 'gl10UzzNdyNSS2n8BJbj7yuNu5f6vfDP')
            json_response = response.json()
            if json_response.get("access_token") != None:
                access_token = json_response['access_token']
                request.session["Authorization"] = access_token
                return Response({'Message':'login berhasil'}, status=status.HTTP_202_ACCEPTED)
        return Response({'Message':'username atau password salah'}, status=status.HTTP_400_BAD_REQUEST)
